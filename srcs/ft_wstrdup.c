/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wstrdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:50 by clrichar          #+#    #+#             */
/*   Updated: 2018/03/10 22:38:32 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

wchar_t				*ft_wstrdup(const wchar_t *ws)
{
	int				i;
	wchar_t			*dest;

	if (!ws)
		return (NULL);
	if (!(dest = ft_wstrnew(ft_wstrlen(ws))))
		return (NULL);
	i = 0;
	while (*(ws + i))
	{
		*(dest + i) = *(ws + i);
		i++;
	}
	*(dest + i) = '\0';
	return (dest);
}
