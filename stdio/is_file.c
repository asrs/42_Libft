/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_file.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/26 23:24:09 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:48:30 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

bool			is_valid(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	return ((ft_strchr("p%", c) != 0) ? true : false);
}

bool			is_int(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	return ((ft_strchr(INT, c) != 0) ? true : false);
}

bool			is_signed(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	return ((ft_strchr("dDi", c) != 0) ? true : false);
}

bool			is_char(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	return ((ft_strchr("cC", c)) ? true : false);
}

bool			is_addr(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	return ((c == 'p') ? true : false);
}
