/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 11:12:18 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:59:08 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "libft.h"
# include <stdarg.h>
# include <stdlib.h>
# include <stdint.h>
# include <stdbool.h>
# include <unistd.h>
# include <limits.h>

# define COLOR_RED     "\x1b[31m"
# define COLOR_GREEN   "\x1b[32m"
# define COLOR_YELLOW  "\x1b[33m"
# define COLOR_BLUE    "\x1b[34m"
# define COLOR_MAGENTA "\x1b[35m"
# define COLOR_CYAN    "\x1b[36m"
# define COLOR_RESET   "\x1b[0m"

# define PLUS			0
# define MINUS			1
# define SPACE			2
# define OCT			3
# define HEX			4
# define BIN			5
# define BUFFER			512
# define POS			dna->index
# define FLAG			dna->flag
# define MODIFER		"%#0- +'"
# define INT			"dDioOuUxXbB"
# define FLAGS			"ABCEDEFGHIJKLMNOPQRSTUVWXYZabcdefgikmnopqrstuvwxy%"
# define UNVALID		"AEFGHIJKLMNPQRTVWYZaegkmnqrtvwy"

typedef union			u_value
{
	long long			tint;
	unsigned long		taddr;
	unsigned long long	tuint;
}						t_value;

typedef struct			s_flag
{
	t_value				value;
	bool				(*f[13])(char *s);
	unsigned char		modifier;
	long				v_sign;
	long				v_size;
	long				v_padding;
	long				len;
	char				*s_sign;
	char				*s_size;
	char				*s_padding;
	char				*s_flag;
}						t_flag;

typedef struct			s_dna
{
	t_flag				flag;
	va_list				va;
	char				buffer[BUFFER + 1];
	size_t				format_len;
	size_t				stocked;
	size_t				index;
	int					ret;
}						t_dna;

extern int				ft_printf(const char *format, ...);
extern void				ft_parse(const char *format);
extern bool				ft_color(const char *format);
extern void				ft_flag(char *s, t_dna *dna);
extern void				ft_buffer(char *s, size_t len);

extern bool				is_type(char *s);
extern bool				is_valid(char *s);
extern bool				is_int(char *s);
extern bool				is_signed(char *s);
extern bool				is_char(char *s);
extern bool				is_addr(char *s);

extern bool				ex_other(char *s);
extern bool				ex_int(char *s);
extern bool				ex_uint(char *s);
extern bool				ex_char(char *s);
extern bool				ex_string(char *s);
extern bool				ex_addr(char *s);
extern bool				ex_percent(char *s);

extern void				get_modifier(char *s, t_dna *dna);
extern void				get_precision(char *s, t_dna *dna);
extern void				set_flag(t_dna *dna);
extern char				*set_sign(int index);
extern char				*set_size(void);
extern char				*set_padding(void);

extern char				*unicode(wchar_t c);
extern t_dna			*call(void);

#endif
