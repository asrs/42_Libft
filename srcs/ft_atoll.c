/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoll.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:34 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:18:42 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

long long			ft_atoll(const char *str)
{
	long long		i;
	long long		neg;

	i = 0;
	neg = 0;
	while (*str && ((*str >= 7 && *str <= 13) || *str == ' '))
		str++;
	if (*str == '+')
		str++;
	else if (*str == '-')
	{
		neg = 1;
		str++;
	}
	while (*str && ft_isdigit(*str))
	{
		i = (i * 10) + ((int)(*str) - '0');
		str++;
	}
	return ((neg == 1) ? -i : i);
}
